<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class,[
                "label" => "Votre Prenom",
                "attr" =>[
                    "placeholder"=>"veuillez entrer le prenom"]
    ])
            ->add('lastName',TextType::class,[
                "label" => "Votre Nom",
                "attr" =>[
                    "placeholder"=>"veuillez entrer le nom"]
    ])
            ->add('email',EmailType::class,[
                "attr" =>[
                    "placeholder"=>"Ex@exemple.com"]
            ])
            ->add('password',RepeatedType::class,[
                "type"=> PasswordType::class,
                "invalid_message" => "le mot de passe et la confirmation doivent etre identiques",
                "label" => "Votre mot de passe",
                "required" => true,
                "first_options" =>["label" => "Votre mot de passe"] ,
                "second_options" => ["label" =>"confirmez le mot de passe"]
            ])
            ->add('submit', SubmitType::class,[
                "label"=>"s'inscrire"
            ])


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
